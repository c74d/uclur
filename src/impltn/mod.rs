
extern crate libc;

use std::char;

struct InfoRow {
    label: &'static str,
    fmt_fn: fn(char) -> String,
}

static INFO_ROWS: [InfoRow; 2] = [InfoRow {
                                      label: "code point",
                                      fmt_fn: fmt_codepoint,
                                  },
                                  InfoRow {
                                      label: "Rust escaped representation",
                                      fmt_fn: fmt_rust_escape,
                                  }];

fn show_codepoint(code_point: u32) -> i32 {
    let unicode_scalar_value = match char::from_u32(code_point) {
        Some(n) => n,
        None => {
            println!("error: the Unicode code-point U+{:08X} is not a Unicode scalar value, and \
                      only Unicode code-points that are Unicode scalar values are supported at \
                      present",
                     code_point);
            return libc::EXIT_FAILURE;
        }
    };

    let max_label_len = INFO_ROWS.iter().map(|row| row.label.len()).max().unwrap_or(0);

    for row in INFO_ROWS.iter() {
        println!("{0:>2$}: {1}",
                 row.label,
                 (row.fmt_fn)(unicode_scalar_value),
                 max_label_len);
    }

    libc::EXIT_SUCCESS
}

pub fn show_codepoint_by_str(code_point_str: &str, radix: u32) -> i32 {
    if radix < 2 || radix > 36 {
        println!("error: value of `--radix` (`-r`) argument must be between 2 and 36 inclusive, \
                  but is {radix_arg}",
                 radix_arg = radix);
        return libc::EXIT_FAILURE;
    }

    let code_point = match u32::from_str_radix(code_point_str, radix) {
        Ok(n) => n,
        Err(err) => {
            println!("error: failed to parse code point as number from string (reason: {err}; \
                      input: {string:?})",
                     err = err,
                     string = code_point_str);
            return libc::EXIT_FAILURE;
        }
    };

    show_codepoint(code_point)
}

fn fmt_codepoint(code_point: char) -> String {
    format!("U+{0:01$X}",
            code_point as u32,
            codepoint_digits_width(code_point))
}

fn fmt_rust_escape(code_point: char) -> String {
    format!("{:?}", code_point)
}

fn codepoint_digits_width(code_point: char) -> usize {
    match code_point as u32 {
        0x0000...0xFFFF => 4,
        _ => 8,
    }
}
