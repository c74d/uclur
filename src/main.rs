
extern crate libc;

extern crate docopt;
extern crate rustc_serialize;

mod impltn;

use std::process;

use docopt::Docopt;

const DOCOPT_TEXT: &'static str = include_str!("docopt.txt");

const VERSION: Option<&'static str> = option_env!("CARGO_PKG_VERSION");

fn main() {
    let status_code = {
        handle_cmdline_args()
    };
    process::exit(status_code);
}

#[derive(RustcDecodable)]
struct Args {
    flag_help: bool,
    flag_version: bool,
    flag_radix: u32,
    cmd_cp: bool,
    arg_codepoint_to_look_up: Option<String>,
}

fn show_help() -> i32 {
    print!("{}", DOCOPT_TEXT);
    libc::EXIT_SUCCESS
}

fn handle_cmdline_args() -> i32 {

    let args = Docopt::new(DOCOPT_TEXT)
                   .and_then(|d| d.version(VERSION.map(|s| format!("uclur v{}", s))).decode())
                   .unwrap_or_else(|err| err.exit());

    match args {
        Args { flag_help: true, .. } => show_help(),
        Args { flag_version: true, .. } => {
            println!("Version not known.");
            libc::EXIT_FAILURE
        }
        Args { cmd_cp: true, arg_codepoint_to_look_up: Some(cp), flag_radix: radix, .. } => {
            impltn::show_codepoint_by_str(&cp, radix)
        }
        Args { .. } => show_help(),
    }
}
